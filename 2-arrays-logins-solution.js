const loginsData = require('./loginsdata');

/* 1. Find all people who are Agender */

const agenders = loginsData.filter((login)=>{
    return login.gender === "Agender";

});

console.log("List of Agenders:")
console.log(agenders);
console.log("\n\n");

/* 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143] */

const ipAdressComponents = loginsData.map((login)=>{
    const ipAddress = login.ip_address;
    const ipComponents = ipAddress.split(".").map((value)=>parseInt(value));
    return ipComponents;
});

console.log("IP components:")
console.log(ipAdressComponents);
console.log("\n\n");

/* 3.1 Find the sum of all the second components of the ip addresses. */

const ipSecondComponentsSum = ipAdressComponents.reduce((acc, cur)=>{
    return acc + cur[1];
}, 0);

console.log("IP address second components :")
console.log(ipSecondComponentsSum);
console.log("\n\n");

/* 3.2 Find the sum of all the fourth components of the ip addresses. */

const ipFouthComponentsSum = ipAdressComponents.reduce((acc, cur)=>{
    return acc + cur[3];
}, 0);

console.log("IP address fourth components :")
console.log(ipFouthComponentsSum);
console.log("\n\n");

/* 4. Compute the full name of each person and store it in a new key (full_name or something) for each person. */

const modifiedData = loginsData.map((login)=>{
    const loginModified = {...login};
    loginModified["full_name"] = `${login.first_name} ${login.last_name}`;
    return loginModified;
});

console.log("MOdified log data:")
console.log(modifiedData);
console.log("\n\n");

// 5. Filter out all the .org emails

const orgEmails = loginsData.filter((login)=>{
    return login.email.endsWith(".org");
}).map((login)=>{
    return login.email;
});

console.log("List of org mails:")
console.log(orgEmails);
console.log("\n\n");

// 6. Calculate how many .org, .au, .com emails are there

const emailStats = loginsData.reduce((acc, cur)=>{
    if(cur.email.endsWith(".org")){
        acc["org"] ++;
    }
    if(cur.email.endsWith(".au")){
        acc["au"] ++;
    }
    if(cur.email.endsWith(".com")){
        acc["com"] ++;
    }
    return acc;

}, {org:0, au: 0, com :0});

console.log("Email stats:")
console.log(emailStats);
console.log("\n\n");


//7. Sort the data in descending order of first name

const sortedData = loginsData.sort((login1, login2)=>{
    return login1.first_name < login2.first_name ? 1 : login1.first_name == login2.first_name ? 0 : -1;
    });
    
    console.log("Sorted data by first name:")
    console.log(sortedData);
    console.log("\n\n");